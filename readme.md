# PHP XMAS HANDLERS

As a geeky Christmas fan I want to generate ASCII shapes of stars and trees in three different sizes so that I can decorate my office nicely for Christmas.

### Installing

#### WEB
Transfer to any directory on your web server supporting PHP 7.2.x.
Launch from the browser https://YOUR_DOMAIN/xmas/.

```
https://YOUR_DOMAIN/xmas/
```
Next, use the standard HTML5 form to execute the request

#### CLI

```
cd xmas
php index.php
php index.php -s
php index.php -l
php index.php -m
```
There are three options for the size of the figures: S, M, L.
Use any of them as an argument.

#### Run without argument

Shapes of randomly selected size will be displayed.

#### Using another language

```
index.php
$model = new Xmas('de');
/classes/lang/de/Xmas.php

<?php
$lang = [
    'init' => 'Willkommen bei den Santa Handlers!',
    'print' => 'DRUCKEN',
    'actionText' =>  'Wählen Sie eine Größe für die XMAS-Figuren:',
    'choseSize' => 'Sie wählen die Größe',
    'choseLines' => 'Gesamtanzahl der Zeilen',
    'size' => 'Größe',
    'linesHeight' => 'Linienhöhe',
    'randomSize' => 'Zufällige Auswahl',
    'anySize' => 'Alle Größen',
    'example' => 'Voorbeeld'
];
```

## Built With

* [JetBrains PHPStorm](https://www.jetbrains.com/phpstorm/promo/) - The best PHP IDE
* [PHP Version 7.4.3](https://www.php.net/releases/7_4_3.php) - STRONG
* [X-Debug v2.9.2](https://xdebug.org/) - For debugging and finding jambs

## Authors

* **Yuri Yuriev** - *Kyto Task* - [YuriYuriev](https://gitlab.com/yurievyuri/)