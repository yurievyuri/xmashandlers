<?php
require_once __DIR__ . '/classes/Xmas.php';
$model = new Xmas('en');

try {

    $model->getForm();
    $model->checkErrors();

    try {

        $model->Magic();
    } catch ( Exception $e ) {

        echo $e->getCode();

    } finally {

        echo $model->printNow();
    }

} catch ( Exception $e ) {
    echo $e->getCode();
}