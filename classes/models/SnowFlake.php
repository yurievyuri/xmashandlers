<?php

class SnowFlake
{
    private object $parent;
    private array $progression;
    public array $model;

    /**
     * SnowFlake constructor.
     *
     * @param $parent
     */
    public function __construct( object $parent )
    {
        if ( !$parent ) {
            return;
        }

        $this->parent = $parent;

        if ( isset($_SERVER[ 'TERM' ]) ) {
            $this->parent->symbols[ 'empty' ] = ' ';
        }
    }

    /**
     * Standard method for forming steps and number of characters
     * @return array
     */
    public function getProgression() : array
    {
        $lines = (int)( $this->parent->lines ? : 5 );
        $globalStep = 4;
        $centerY = round(( $lines / 2 ), 0, PHP_ROUND_HALF_DOWN);
        $arLines = [];
        $arLines[ 'UP_STAR' ] = 1;
        for ( $i = 1; $i <= $centerY; $i++ ) {
            if ( $i === 1 ) {
                $arLines[] = $i;
                continue;
            }

            if ( $lines > 7 && $i === 2 ) {
                $arLines[] = 3;
                continue;
            }

            $arLines[ $centerY == $i ? 'CENTER' : $i ] = end($arLines) + $globalStep;
        }
        $tempArray = $arLines;
        unset($tempArray[ 'CENTER' ]);
        $arLines = array_merge($arLines, array_reverse($tempArray));
        $arLines[ 'DOWN_STAR' ] = 1;

        if ( $this->parent && count($arLines) != $lines ) {
            $this->parent->addError(__METHOD__ . ' - calculated lines do not agree with the condition');
        }

        return $this->progression = $arLines;
    }

    /**
     * The standard method for handling steps, filling in blank and valid characters
     * @return array
     */
    public function prepareModel() : array
    {
        if ( $this->parent && !$this->progression ) {
            $this->parent->addError(__METHOD__ . ' - unable to create a model of the figure, input parameters are insufficient');
        }

        $stepArray = $this->progression;
        $stepArray[ 'CENTER' ] += 2;
        $snowFlakeArray = [];

        foreach ( $stepArray as $step => $elements ) {
            $emptyString = str_repeat($this->parent->symbols[ 'empty' ], ( $stepArray[ 'CENTER' ] - $elements ) / 2);

            $snowFlakeArray[ $step ][ 'silent' ] = isset($_SERVER[ 'TERM' ]) ? $emptyString : '<span style="color:white;">' . $emptyString . '</span>';
            $snowFlakeArray[ $step ][ 'string' ] = str_repeat(strripos($step, '_STAR') ? $this->parent->symbols[ 'star' ] : $this->parent->symbols[ 'point' ], (int)$elements);
            if ( $step === 'CENTER' ) {
                $snowFlakeArray[ $step ][ 'string' ] = substr_replace($snowFlakeArray[ $step ][ 'string' ], $this->parent->symbols[ 'star' ], -1);
                $snowFlakeArray[ $step ][ 'string' ] = substr_replace($snowFlakeArray[ $step ][ 'string' ], $this->parent->symbols[ 'star' ], 0, 1);
            }
        }

        return $this->model = $snowFlakeArray;
    }
}