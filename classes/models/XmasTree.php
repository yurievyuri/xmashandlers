<?php

class XmasTree
{
    private object $parent;
    private array $progression;
    public array $model;

    /**
     * XmasTree constructor.
     *
     * @param $parent
     */
    public function __construct( object $parent )
    {
        if ( !$parent ) {
            return;
        }
        $this->parent = $parent;
        if ( isset($_SERVER[ 'TERM' ]) ) {
            $this->parent->symbols[ 'empty' ] = ' ';
        }
    }

    /**
     * Standard method for forming steps and number of characters
     * @return array
     */
    public function getProgression() : array
    {
        $lines = (int)( $this->parent->lines ? : 5 );
        $arLines = [];

        for ( $i = -1; $i <= ( $lines - 2 ); $i++ ) {
            if ( $i < 1 ) {
                $arLines[] = 1;
                continue;
            }

            $arLines[] = 2 * $i + 1;
        }

        return $this->progression = $arLines;
    }

    /**
     * The standard method for handling steps, filling in blank and valid characters
     * @return array
     */
    public function prepareModel() : array
    {
        if ( $this->parent && !$this->progression ) {
            $this->parent->addError(__METHOD__ . ' - unable to create a model of the figure, input parameters are insufficient');
        }

        $treeArray = [];
        $stepArray = $this->progression;
        $centerX = round(( end($stepArray) / 2 ), 0, PHP_ROUND_HALF_DOWN);

        foreach ( $stepArray as $step => $elements ) {
            if ( (int)$elements > 1 ) {
                $centerX--;
            }

            $emptyString = str_repeat($this->parent->symbols[ 'empty' ], $centerX);
            $treeArray[ $step ][] = isset($_SERVER[ 'TERM' ]) ? $emptyString : '<span style="color:white;">' . $emptyString . '</span>';

            if ( $step === 0 ) {
                $treeArray[ $step ][] = $this->parent->symbols[ 'star' ];
            }
            $treeArray[ $step ][] = $step > 0 ? str_repeat($this->parent->symbols[ 'point' ], (int)$elements) : '';
        }

        return $this->model = $treeArray;
    }
}