<?php
require_once 'Lang.php';

class Xmas
{
    private string $init;
    private string $actionText;
    public string $lang = 'en';
    public array $error = [];
    public string $lunchType;
    private array $availableSizes = [
        5 => 'S',
        7 => 'M',
        11 => 'L'
    ];
    public string $lines = 'ANY';
    private string $preg = '/[^a-zA-Z]/ui';
    private string $path = __DIR__ . '/models/';
    public array $symbols = [
        'empty' => 'X',
        'star' => '+',
        'point' => 'X'
    ];
    private string$separator;
    private Lang $lng;
    private array $prepareMethods = [ 'getProgression', 'prepareModel' ];
    private string $html = '';
    private array $print = [];

    /**
     * Xmas constructor.
     *
     * @param string $lang
     */
    public function __construct( $lang = 'en' )
    {
        $this->setLang($lang);
        $this->lng = new Lang($this);
        $this->init = $this->lng->getString('init', __FILE__,);
        $this->actionText = $this->lng->getString('actionText', __FILE__);
        $this->separator = $this->getLunchType() === 'cli' ? "\n" : '<br>';
    }

    /**
     * Getting a random size
     * @return mixed
     */
    public function getRandomSize()
    {
        return array_rand($this->availableSizes);
    }

    /**
     * Checking the availability of the requested size
     *
     * @param null $size
     *
     * @return false|int|string
     */
    public function checkSizeAvailability( $size = null )
    {
        return array_search(
            preg_replace($this->preg, '', strtoupper($size)),
            $this->availableSizes,
            true
        );
    }

    /**
     * Determining Lunch Type
     * @return string
     */
    public function getLunchType() : string
    {
        return $this->lunchType = isset($_SERVER[ 'TERM' ]) ? 'cli' : 'web';
    }

    /**
     *  The choice of the form of data output to the screen with the output
     * @throws Exception
     */
    public function getForm()
    {
        if ( isset($_POST[ 'lines' ]) && $_POST[ 'lines' ] === 'ANY' ) {
            $_POST[ 'lines' ] = $this->getRandomSize();
        }

        if ( $this->getLunchType() === 'web' ) {
            $this->lines = $_POST[ 'lines' ] ?? $this->getRandomSize();

            echo $this->webForm();
        } else {
            if ( isset($_SERVER[ 'argv' ][ 1 ]) ) {
                $this->lines = array_search(
                    strtoupper(preg_replace($this->preg, '', $_SERVER[ 'argv' ][ 1 ])),
                    $this->availableSizes,
                    true
                ) ? : $this->getRandomSize();
            } else {
                $this->lines = $this->getRandomSize();
            }

            echo $this->cliForm();
        }
    }

    /**
     * Preparing a screen for displaying on a browser screen
     * @return string
     * @throws Exception
     */
    public function webForm() : string
    {
        if ( !$this->availableSizes ) {
            $this->addError(__METHOD__ . ' not enough data to create a settings list');
        }

        $this->html .= '<form action="./" method="post">';

        $this->html .= '<h2>' . $this->init . '</h2>';

        $this->html .= '<h5>' . $this->actionText . '</h5>';

        $this->html .= '<select name="lines">';

        $this->html .= '<option value="ANY">' . $this->lng->getString('anySize', __FILE__) . '</option>';

        foreach ( $this->availableSizes as $lines => $size ) {
            $this->html .= '<option ';

            if ( $this->lines == $lines ) {
                $this->html .= 'selected="selected"';
            }

            $this->html .= ' value="' . $lines . '"> ' . $this->lng->getString('size', __FILE__) . ' [' . $size . '] : ' . $lines . ' ' . $this->lng->getString('linesHeight', __FILE__) . '</option>';
        }

        $this->html .= '</select><br><br>';

        $this->html .= '<button type="submit">' . $this->lng->getString('print', __FILE__) . '</button></form>';

        return trim($this->html);
    }

    /**
     * Preparation of data output to the terminal screen
     * @return string
     * @throws Exception
     */
    public function cliForm() : string
    {
        if ( !$this->availableSizes ) {
            $this->addError(__METHOD__ . ' not enough data to create a settings list');
        }

        system('clear');

        $cli[] = "\e[93m\e[1m" . $this->init;
        $cli[] = ' ';

        $cli[] = $this->actionText;
        $cli[] = ' ';

        foreach ( $this->availableSizes as $lines => $size ) {
            $cli[] = $this->lng->getString('size', __FILE__) . ' [' . $size . '] : ' . $lines . ' ' . $this->lng->getString('linesHeight', __FILE__);
        }

        $cli[] = ' ';
        $cli[] = "\e[93m" . ' ***** ' . $this->lng->getString('example', __FILE__) . ': $ php index.php -M [-S] [-L]';
        $cli[] = ' ';

        if ( isset($_SERVER[ 'argv' ][ 1 ]) && $this->checkSizeAvailability($_SERVER[ 'argv' ][ 1 ]) ) {
            $cli[] = "\e[32m" . $this->lng->getString('choseSize', __FILE__) . ': ' . preg_replace(
                    $this->preg, '', strtoupper($_SERVER[ 'argv' ][ 1 ])
                );
            $cli[] = "\e[32m" . $this->lng->getString('choseLines', __FILE__) . ': ' . $this->lines;
            $cli[] = ' ';
        } else {
            $cli[] = $this->lng->getString('randomSize', __FILE__);
            $cli[] = $this->lng->getString('choseLines', __FILE__) . ': ' . $this->lines;
        }

        $cli[] = ' ';
        $cli[] = ' ';

        return implode("\e[0m\n", $cli);
    }

    /**
     * Connecting classes serving different shapes
     * To add a new figure, you need to create a new file with the class of the same name in the models / folder
     * @return bool
     * @throws Exception
     */
    public function Magic() : bool
    {
        if ( !$this->lines ) {
            $this->addError(__METHOD__ . ' could not get the number of lines for calculations');
            return false;
        }

        $filesArray = glob($this->path . '*.php');

        if ( empty($filesArray) ) {
            $this->addError(__METHOD__ . ' could not get the list of files at the specified path : ' . $this->path);
            return false;
        }

        foreach ( $filesArray as $filename ) {
            if ( !require( $filename ) ) {
                $this->addError(__METHOD__ . ' не удалось получить файл ' . $filename);
                continue;
            }

            if ( !$className = basename($filename, '.php') ) {
                $this->addError(__METHOD__ . ' could not get file name [' . $className . ']');
                continue;
            }

            if ( !class_exists($className) ) {
                $this->addError(__METHOD__ . ' could not match class with file name [' . $className . ']');
                continue;
            }

            $this->{$className} = new $className($this);

            foreach ( $this->prepareMethods as $method ) {
                if ( !method_exists($this->{$className}, $method) ) {
                    $this->addError(__METHOD__ . ' could not find the required class ' . $className . '::' . $method);
                    continue;
                }

                $this->{$className}->{$method}();
            }

            if ( $this->{$className}->model ) {
                $this->prepareToPrint($this->{$className});
            }
        }

        return true;
    }

    /**
     * Displaying the results obtained from figure models
     *
     * @param $object
     *
     * @throws Exception
     */
    private function prepareToPrint( $object ) : void
    {
        if ( !$object->model ) {
            $this->addError(__METHOD__ . ' failed to get shape module for display');
            return;
        }

        $this->print[] = $this->separator;

        foreach ( $object->model as $step => $string ) {
            foreach ( $string as $line ) {

                $this->print[] = str_replace('0', ' ', $line);
            }

            $this->print[] = $this->separator;
        }

        $this->print[] = $this->separator;
    }

    /**
     * @return string
     */
    public function printNow()
    {
        return $this->print ? implode('', $this->print) : 'No data collected for printing';
    }

    /**
     * Error checking and display
     * @return bool
     */
    public function checkErrors()
    {
        if ( !$this->getError() ) {
            return true;
        }

        echo $this->lng->getString('errors', __FILE__);
        echo $this->separator;
        echo implode($this->separator, $this->getError());
        echo $this->separator;

        return false;
    }

    /**
     * @return array
     */
    public function getError() : array
    {
        return $this->error;
    }

    /**
     * @param string $string
     *
     * @throws Exception
     */
    public function addError( string $string )
    {
        $this->error[] = $string;

        throw new \Exception($string);
    }

    /**
     * @return string
     */
    public function getLang() : string
    {
        if ( !$this->lang ) {
            $this->lang = 'en';
        }
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang( string $lang ) : void
    {
        $this->lang = $lang;
    }

    /**
     * @return string
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @param null $lines
     */
    public function setLines( $lines = null )
    {
        $this->lines = $lines;
    }
}