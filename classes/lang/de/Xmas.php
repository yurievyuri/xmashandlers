<?php
$arLang = [
    'init' => 'Willkommen bei den Santa Handlers!',
    'print' => 'DRUCKEN',
    'actionText' =>  'Wählen Sie eine Größe für die XMAS-Figuren:',
    'choseSize' => 'Sie wählen die Größe',
    'choseLines' => 'Gesamtanzahl der Zeilen',
    'size' => 'Größe',
    'linesHeight' => 'Linienhöhe',
    'randomSize' => 'Zufällige Auswahl',
    'anySize' => 'Alle Größen',
    'example' => 'Voorbeeld',
    'errors' => 'De volgende fouten zijn gedetecteerd tijdens het uitvoeren van het programma:'
];