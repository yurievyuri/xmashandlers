<?php
$arLang = [

    'init' => 'Welcome to the Santa Handlers!',
    'print' => 'PRINT',
    'actionText' =>  'Select a size for the XMAS figures:',
    'choseSize' => 'You chose size',
    'choseLines' => 'Total lines',
    'size' => 'Size',
    'linesHeight' => 'lines height',
    'randomSize' => 'Random selection',
    'anySize' => 'Any size',
    'example' => 'Example',
    'errors' => 'The following errors were detected during program execution:'
];