<?php

/**
 * @property string lang
 */
class Lang
{
    public const dirName = 'lang';
    private object $parent;

    public function __construct( $parent )
    {
        $this->parent = $parent;
    }

    /**
     * @param string $key
     * @param string $file
     *
     * @return string
     */
    public function getString( $key, string $file ) : string
    {
        if ( !$key || !$file ) {
            $this->parent->addError(__METHOD__ . ' - failed to get valid data');

            return '';
        }

        $arLang = [];

        if ( require $this->getLangFile($file) ) {
            return $arLang[ $key ];
        }

        return 'could not get text for this key';
    }

    /**
     * Method for obtaining language files
     *
     * @param string $path
     *
     * @return bool|string
     */
    private function getLangFile( string $path )
    {
        $array = pathinfo($path);
        $langFile = $array[ 'dirname' ] . '/' . self::dirName . '/' . $this->parent->lang . '/' . $array[ 'basename' ];
        if ( file_exists($langFile) ) {
            return $langFile;
        }
        return false;
    }
}